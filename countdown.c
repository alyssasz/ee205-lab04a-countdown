///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   Reference time: Tue Jan 21 04:26:07 PM HST 2014
//
// @author Alyssa Zhang <alyssasz@hawaii.edu>
// @date   Feb 4 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

void convertTime(double secDiff) {
   
   int secInMin = 60;
   int secInHour = 60 * secInMin;
   int secInDay = 24 * secInHour;
   int secInYear = 365 * secInDay;

   // converts secs into terms of years, days, hours, mins, and secs
   long yearDiff = (long) secDiff / secInYear;
   secDiff = secDiff - (yearDiff*secInYear);
   
   long dayDiff = (long) secDiff / secInDay;
   secDiff = secDiff - (dayDiff*secInDay);
   
   long hourDiff = (long) secDiff / secInHour;
   secDiff = secDiff - (hourDiff*secInHour);
   
   long minDiff = (long) secDiff / secInMin;
   secDiff = secDiff - (minDiff*secInMin);

   printf("Years: %ld Days: %ld Hours: %ld Minutes: %ld Seconds: %.f\n", yearDiff, dayDiff, hourDiff, minDiff, secDiff); 
  

}


int main(int argc, char* argv[]) {

   char buf[200];

   tzset(); // sets the timezone info
   struct tm referenceTime = { 7, 26, 16, 21, 0, 114, 2,};
   
   // formats broken-down time into a data string and prints
   strftime(buf, 200, "Reference time: %a %b %d %r %Z %G\n", &referenceTime);
   puts(buf);

   time_t refTime = mktime(&referenceTime);
   time_t now = time(&now);

   // calculates the difference btwn the current time and reference time
   double secDiff = difftime(now,refTime);

   while(1) {

      convertTime(secDiff);
      secDiff = secDiff - 1; //count down by sec

      sleep(1); //pauses for one sec

   }

   return 0;

}
